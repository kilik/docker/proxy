.PHONY: help

## Display this help text
help:
	$(info ---------------------------------------------------------------------)
	$(info -                        Available targets                          -)
	$(info ---------------------------------------------------------------------)
	@awk '/^[a-zA-Z\-\_0-9]+:/ {                                                \
	nb = sub( /^## /, "", helpMsg );                                            \
	if(nb == 0) {                                                               \
		helpMsg = $$0;                                                      \
		nb = sub( /^[^:]*:.* ## /, "", helpMsg );                           \
	}                                                                           \
	if (nb)                                                                     \
		printf "\033[1;31m%-" width "s\033[0m %s\n", $$1, helpMsg;          \
	}                                                                           \
	{ helpMsg = $$0 }'                                                          \
	$(MAKEFILE_LIST) | column -ts:

# auto configure .env
.env: .env.dist
	cp .env.dist .env

# auto select docker-compose
docker-compose.yml:
	ln -s docker-compose.simple.yml docker-compose.yml

## autoconfig
autoconfig: .env docker-compose.yml

## pull last dependencies
pull: autoconfig
	docker-compose pull

## start the proxy service
start: autoconfig
	./start.sh

## up (start alias)
up: start

## stop the proxy service
stop:
	docker-compose stop

## remove the proxy service
down:
	docker-compose down --remove-orphans --volumes

## restart the proxy service
restart: stop start

## show logs
logs:
	docker-compose logs -f --tail=1000

## show status
ps:
	docker-compose ps

## upgrade (and restart) the proxy
upgrade: pull restart

## reload configuration (restart gen)
reload:
	docker-compose stop gen
	sleep 2
	docker-compose up -d
	sleep 2
	docker-compose ps
