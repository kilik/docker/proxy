Install a Nginx reverse Proxy with Letsencerypt
===============================================

This stack run nginx as reverse proxy.
VIRTUAL_HOST env var is used in projects to route HTTP(s) requests to good service.

Requirements:
* docker
* docker compose
* make

Install this project
====================

```shell
git clone git@gitlab.com:/kilik/docker/proxy.git
cd proxy
make upgrade
```

Display help
```shell
make
```

Use SSL version
===============

```shell
make stop
rm docker-compose.yml
ln -s docker-compose.ssl.yml docker-compose.yml
make start
```

Prod ready ?

Don't forget to disable staging, editing your .env:

```
# default setup: staging:
# ACME_CA_URI=https://acme-staging.api.letsencrypt.org/directory

# for prod setup use this:
ACME_CA_URI=https://acme-v01.api.letsencrypt.org/directory
```
